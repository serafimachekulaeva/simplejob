/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.google.android.gms;

public final class R {
    private R() {}

    public static final class attr {
        private attr() {}

        public static final int ambientEnabled = 0x7f04002a;
        public static final int buttonSize = 0x7f040044;
        public static final int cameraBearing = 0x7f040049;
        public static final int cameraMaxZoomPreference = 0x7f04004a;
        public static final int cameraMinZoomPreference = 0x7f04004b;
        public static final int cameraTargetLat = 0x7f04004c;
        public static final int cameraTargetLng = 0x7f04004d;
        public static final int cameraTilt = 0x7f04004e;
        public static final int cameraZoom = 0x7f04004f;
        public static final int circleCrop = 0x7f040058;
        public static final int colorScheme = 0x7f04006d;
        public static final int imageAspectRatio = 0x7f0400c5;
        public static final int imageAspectRatioAdjust = 0x7f0400c6;
        public static final int latLngBoundsNorthEastLatitude = 0x7f0400d2;
        public static final int latLngBoundsNorthEastLongitude = 0x7f0400d3;
        public static final int latLngBoundsSouthWestLatitude = 0x7f0400d4;
        public static final int latLngBoundsSouthWestLongitude = 0x7f0400d5;
        public static final int liteMode = 0x7f040118;
        public static final int mapType = 0x7f04011b;
        public static final int scopeUris = 0x7f040140;
        public static final int uiCompass = 0x7f0401a7;
        public static final int uiMapToolbar = 0x7f0401a8;
        public static final int uiRotateGestures = 0x7f0401a9;
        public static final int uiScrollGestures = 0x7f0401aa;
        public static final int uiTiltGestures = 0x7f0401ab;
        public static final int uiZoomControls = 0x7f0401ac;
        public static final int uiZoomGestures = 0x7f0401ad;
        public static final int useViewLifecycle = 0x7f0401af;
        public static final int zOrderOnTop = 0x7f0401bb;
    }
    public static final class color {
        private color() {}

        public static final int common_google_signin_btn_text_dark = 0x7f06004d;
        public static final int common_google_signin_btn_text_dark_default = 0x7f06004e;
        public static final int common_google_signin_btn_text_dark_disabled = 0x7f06004f;
        public static final int common_google_signin_btn_text_dark_focused = 0x7f060050;
        public static final int common_google_signin_btn_text_dark_pressed = 0x7f060051;
        public static final int common_google_signin_btn_text_light = 0x7f060052;
        public static final int common_google_signin_btn_text_light_default = 0x7f060053;
        public static final int common_google_signin_btn_text_light_disabled = 0x7f060054;
        public static final int common_google_signin_btn_text_light_focused = 0x7f060055;
        public static final int common_google_signin_btn_text_light_pressed = 0x7f060056;
        public static final int common_google_signin_btn_tint = 0x7f060057;
    }
    public static final class drawable {
        private drawable() {}

        public static final int common_full_open_on_phone = 0x7f080077;
        public static final int common_google_signin_btn_icon_dark = 0x7f080078;
        public static final int common_google_signin_btn_icon_dark_focused = 0x7f080079;
        public static final int common_google_signin_btn_icon_dark_normal = 0x7f08007a;
        public static final int common_google_signin_btn_icon_dark_normal_background = 0x7f08007b;
        public static final int common_google_signin_btn_icon_disabled = 0x7f08007c;
        public static final int common_google_signin_btn_icon_light = 0x7f08007d;
        public static final int common_google_signin_btn_icon_light_focused = 0x7f08007e;
        public static final int common_google_signin_btn_icon_light_normal = 0x7f08007f;
        public static final int common_google_signin_btn_icon_light_normal_background = 0x7f080080;
        public static final int common_google_signin_btn_text_dark = 0x7f080081;
        public static final int common_google_signin_btn_text_dark_focused = 0x7f080082;
        public static final int common_google_signin_btn_text_dark_normal = 0x7f080083;
        public static final int common_google_signin_btn_text_dark_normal_background = 0x7f080084;
        public static final int common_google_signin_btn_text_disabled = 0x7f080085;
        public static final int common_google_signin_btn_text_light = 0x7f080086;
        public static final int common_google_signin_btn_text_light_focused = 0x7f080087;
        public static final int common_google_signin_btn_text_light_normal = 0x7f080088;
        public static final int common_google_signin_btn_text_light_normal_background = 0x7f080089;
        public static final int googleg_disabled_color_18 = 0x7f080092;
        public static final int googleg_standard_color_18 = 0x7f080093;
    }
    public static final class id {
        private id() {}

        public static final int adjust_height = 0x7f0a0021;
        public static final int adjust_width = 0x7f0a0022;
        public static final int auto = 0x7f0a002d;
        public static final int button = 0x7f0a003a;
        public static final int center = 0x7f0a0044;
        public static final int dark = 0x7f0a0065;
        public static final int email = 0x7f0a0076;
        public static final int hybrid = 0x7f0a0097;
        public static final int icon_only = 0x7f0a009a;
        public static final int light = 0x7f0a00a9;
        public static final int none = 0x7f0a00c6;
        public static final int normal = 0x7f0a00c7;
        public static final int progressBar = 0x7f0a00dd;
        public static final int radio = 0x7f0a00e2;
        public static final int satellite = 0x7f0a00e7;
        public static final int standard = 0x7f0a0110;
        public static final int terrain = 0x7f0a011f;
        public static final int text = 0x7f0a0120;
        public static final int text2 = 0x7f0a0121;
        public static final int toolbar = 0x7f0a0135;
        public static final int wide = 0x7f0a017b;
        public static final int wrap_content = 0x7f0a017f;
    }
    public static final class integer {
        private integer() {}

        public static final int google_play_services_version = 0x7f0b0006;
    }
    public static final class string {
        private string() {}

        public static final int common_google_play_services_enable_button = 0x7f11004c;
        public static final int common_google_play_services_enable_text = 0x7f11004d;
        public static final int common_google_play_services_enable_title = 0x7f11004e;
        public static final int common_google_play_services_install_button = 0x7f11004f;
        public static final int common_google_play_services_install_text = 0x7f110050;
        public static final int common_google_play_services_install_title = 0x7f110051;
        public static final int common_google_play_services_notification_ticker = 0x7f110052;
        public static final int common_google_play_services_unknown_issue = 0x7f110053;
        public static final int common_google_play_services_unsupported_text = 0x7f110054;
        public static final int common_google_play_services_update_button = 0x7f110055;
        public static final int common_google_play_services_update_text = 0x7f110056;
        public static final int common_google_play_services_update_title = 0x7f110057;
        public static final int common_google_play_services_updating_text = 0x7f110058;
        public static final int common_google_play_services_wear_update_text = 0x7f110059;
        public static final int common_open_on_phone = 0x7f11005a;
        public static final int common_signin_button_text = 0x7f11005b;
        public static final int common_signin_button_text_long = 0x7f11005c;
    }
    public static final class styleable {
        private styleable() {}

        public static final int[] LoadingImageView = { 0x7f040058, 0x7f0400c5, 0x7f0400c6 };
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] MapAttrs = { 0x7f04002a, 0x7f040049, 0x7f04004a, 0x7f04004b, 0x7f04004c, 0x7f04004d, 0x7f04004e, 0x7f04004f, 0x7f0400d2, 0x7f0400d3, 0x7f0400d4, 0x7f0400d5, 0x7f040118, 0x7f04011b, 0x7f0401a7, 0x7f0401a8, 0x7f0401a9, 0x7f0401aa, 0x7f0401ab, 0x7f0401ac, 0x7f0401ad, 0x7f0401af, 0x7f0401bb };
        public static final int MapAttrs_ambientEnabled = 0;
        public static final int MapAttrs_cameraBearing = 1;
        public static final int MapAttrs_cameraMaxZoomPreference = 2;
        public static final int MapAttrs_cameraMinZoomPreference = 3;
        public static final int MapAttrs_cameraTargetLat = 4;
        public static final int MapAttrs_cameraTargetLng = 5;
        public static final int MapAttrs_cameraTilt = 6;
        public static final int MapAttrs_cameraZoom = 7;
        public static final int MapAttrs_latLngBoundsNorthEastLatitude = 8;
        public static final int MapAttrs_latLngBoundsNorthEastLongitude = 9;
        public static final int MapAttrs_latLngBoundsSouthWestLatitude = 10;
        public static final int MapAttrs_latLngBoundsSouthWestLongitude = 11;
        public static final int MapAttrs_liteMode = 12;
        public static final int MapAttrs_mapType = 13;
        public static final int MapAttrs_uiCompass = 14;
        public static final int MapAttrs_uiMapToolbar = 15;
        public static final int MapAttrs_uiRotateGestures = 16;
        public static final int MapAttrs_uiScrollGestures = 17;
        public static final int MapAttrs_uiTiltGestures = 18;
        public static final int MapAttrs_uiZoomControls = 19;
        public static final int MapAttrs_uiZoomGestures = 20;
        public static final int MapAttrs_useViewLifecycle = 21;
        public static final int MapAttrs_zOrderOnTop = 22;
        public static final int[] SignInButton = { 0x7f040044, 0x7f04006d, 0x7f040140 };
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;
    }
}
